import express from 'express';
import { getUsers, createUser, editUser } from '../controllers/user.controller';
import UserMiddleware from '../middlewares/user.middleware';

const router = express.Router()
const middleware = new UserMiddleware()

router.get("/", getUsers)
router.post("/", middleware.isExistUser, createUser)
router.patch('/:id', middleware.isExistUser, editUser)

export default router