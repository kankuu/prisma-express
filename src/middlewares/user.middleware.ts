import {NextFunction, Request, Response} from 'express';
import { PrismaClient } from '@prisma/client';
const User = new PrismaClient().users

export default class UserMiddleware {
  async isExistUser(req: Request, res: Response, next:NextFunction) {
    const Exist = await User.findFirst({
      where: {
        username: req.body.username
      }
    })

    if (Exist) {
      res.status(400).json({
        message: "user is already exist"
      })
    } else {
      next()
    }
  }
}