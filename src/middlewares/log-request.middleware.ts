import * as express from 'express';

export default class LogRequestMiddleware {
  async loggerMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
    console.log(`${request.method} ${request.path}`);
    next();
  }
}