import { PrismaClient } from '@prisma/client';
import UsersModel from '../models/user.model';
import CommonUtils from '../utils/common.utils';


const User = new PrismaClient().users
const commonUtils = new CommonUtils()

export const UserFindAll = async () => {
  const users = await User.findMany({
    select: {
      id: true,
      username: true
    }
  });
  return users;
}

export const UserCreate = async (user:UsersModel) => {
  return await User.create({
    data: {
      username: user.username,
      password: await commonUtils.hashPassword(user.password)
    }
  })
}

export const UserEdit = async (userId: number, user:UsersModel) => {
  user.password = await commonUtils.hashPassword(user.password)
  return await User.update({
    where: {id: userId},
    data: {
      ...user
    }
  })
}

export const UserValidate = async (user:UsersModel) => {
  const Exist = await User.findFirst({
    where: {username: user.username}
  })

  if (Exist) {

  }
}