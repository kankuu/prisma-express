import swaggerAutogen from 'swagger-autogen';
import dotenv from 'dotenv'

const outputFile = './swagger.json';
const endpointsFiles = './routers/index.ts';
dotenv.config();
const env:any = process.env

const swaggerOptions = {
  info: {
    title: 'Prisma Express',
    version: '1.0.0',
  },
  host: `http://${env.APP_HOST}:${env.APP_PORT}`,
  basePath: '/',
  schemes: ['http'],
};

swaggerAutogen()(outputFile, endpointsFiles, swaggerOptions).then(() => {
  console.log('Swagger file generated');
});