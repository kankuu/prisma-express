import express from 'express';
import dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';

import LogRequestMiddleware from './middlewares/log-request.middleware';
import indexRouter from './routers'
import swaggerDocument from './swagger.json'

dotenv.config();
const env:any = process.env

const app = express();

app.use(bodyParser.json());
app.use(new LogRequestMiddleware().loggerMiddleware)

app.use(indexRouter);

if (process.env.NODE_ENV != 'production') {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}

app.listen(env?.APP_PORT,env?.APP_HOST, () => {
  console.log(`[${env?.NODE_ENV}] Server started on port http://${env.APP_HOST}:${env.APP_PORT}`);
});