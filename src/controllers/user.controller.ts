import {Request, Response} from 'express';
import {
  UserFindAll,
  UserCreate,
  UserEdit
} from '../services/user.service';
import UsersModel from '../models/user.model';

export const getUsers = async (req:Request, res:Response) => {
  try {
    const users = await UserFindAll()
    res.json({
      message: "list users",
      data : users
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ 
      message: 'Server Error' 
    });
  }
};

export const createUser = async(req:Request, res:Response) => {
  try {
    const user:UsersModel = req.body
    const newUser = await UserCreate(user)

    delete newUser.password

    res.json({
      message: "successfull create user",
      data: newUser
    })

  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: "server error"
    })
  }
}

export const editUser =  async(req:Request, res:Response) => {
  try {
    const userId = req.params.id
    const user:UsersModel = req.body
    const editUser = await UserEdit(+userId, user)

    delete editUser.username

    res.json({
      message: "user updated!",
      data: editUser
    })
  } catch (error) {
    console.error(error)
    res.status(500).json({
      message: "server error"
    })
  }
}