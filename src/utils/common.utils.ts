import * as bcrypt from 'bcrypt'

export default class CommonUtils {
  async hashPassword (password:string) {
    let hash = await bcrypt.hash(password, 10)
    return hash
  }
}